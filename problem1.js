const fs = require('fs');

function makefiles(n)
{
    return new Promise((resolve,reject) => {
        if(n > 0)
        {
            resolve(() => {
                for(let value=1; value<=n; value++)
                {
                    let filename = "file"+value+".txt";
                    fs.open(filename, 'w', (err) => {
                    if(err)
                    {
                        console.log(err)
                    }
                    else
                    {
                        console.log(filename+" created successfully");
                    }
                    })
                }
            })
        }
        else
        {
            reject("File cant be created");
        }
    })   
}

function deletefiles(n)
{
    return new Promise((resolve,reject) => {
        if(n > 0)
        {
            resolve(() => {
                for(let value=1; value<=n; value++)
                {
                    let filename = "file"+value+".txt";
                    fs.unlink(filename, (err) => {
                    if(err)
                    {
                         console.log(err);
                    }
                    else
                    {
                        console.log("File Deleted " + filename);
                    }
                    })
                }
        })
        }
        else
        {
            reject("File cant be deleted");
        }
    })
    return p2;
}

module.exports = {makefiles, deletefiles}
