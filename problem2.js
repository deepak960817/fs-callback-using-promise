const fs = require('fs');

function read(filename)
{
    return new Promise((resolve, reject) => {
        fs.readFile(filename, 'utf-8', (err,data) => {
            if(err)
            {
                reject(err);
            }
            else
            {
                //console.log(data);
                resolve(data);
            }
        })
    })
}

function write(filename, info)
{
    return new Promise((resolve, reject) => {
        fs.writeFile(filename, info, (err) => {
            if(err)
            {
                reject(err);
            }
            else
            {
                //console.log(info)
                resolve(filename);
            }
        })
    })
}

function writefilename(filename)
{
    return new Promise((resolve, reject) => {
        fs.appendFile('filenames.txt', filename, (err) => {
            if(err)
            {
                reject(err);
            }
            else
            {
                resolve(filename.trim());
            }
        })
    })
}


function deletefiles(array)
{
    //console.log(array)
    return new Promise((resolve, reject) => {
        if(array.length !== 0)
        {
            resolve(() => {
                for(let value=0; value<array.length; value++)
                {
                    let filename = array[value];
                    
                    fs.unlink(filename, (err) => {
                    if(err)
                    {
                         console.log(err);
                    }
                    else
                    {
                        console.log("File Deleted " + filename);
                    }
                    })
                }     
        })   
        }
        else
        {
            reject("File cant be deleted");
        }
    })
}


module.exports = {read, write, writefilename, deletefiles};


