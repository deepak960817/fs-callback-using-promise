const problem1 = require('../problem1');
const makefiles = problem1.makefiles;
const deletefiles = problem1.deletefiles;

makefiles(4)

.then((data) => {
    data();
    return deletefiles(4);
})
.then((data) => {
   data();
})
.catch((err) => console.log(err));