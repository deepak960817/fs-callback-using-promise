const problem2 = require('../problem2');
const read = problem2.read;
const write = problem2.write;
const writefilename = problem2.writefilename;
const deletefiles = problem2.deletefiles;

const myPromise = read('./lipsum.txt');

myPromise
.then((data) => {
    const uppercaseData = data.toUpperCase();
    return write('Data_In_Uppercase.txt', uppercaseData);
})
.then((file) => {
    // console.log(file);
    // console.log('\n')
    return writefilename(file);
})
.then((file) => {
    return read(file);
})
.then((readData) => {
    
    const lowerCaseData = readData.toLowerCase();
    let splitData = lowerCaseData.split(".");
    return write('Data_In_LowerCase.txt', JSON.stringify(splitData));
})
.then((file) => {
    //console.log(file);
    return writefilename(" "+file);
})
.then((file) => {
    return read(file);
})
.then((content) => {
    toBeSorted = Array.of(content);
    let sortedVal = toBeSorted.sort();
    sortedVal = sortedVal.toString();
    return write('sortedData.txt', sortedVal);
})
.then((file) => {
    //console.log(file);
    return writefilename(" "+file);
})
.then((file) => {
    //console.log(file)
    return writefilename(' filenames.txt');
})
.then((file) => {
    return read('filenames.txt');
})
.then((names) => {
    names = names.split(" ");
    //console.log(Array.isArray(names));
    return deletefiles(names);
})
.then((del) => {
    del();
})
.catch(() => "Error, Couldn't perform the complete operation")
